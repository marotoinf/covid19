# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import sys
import requests
import json
import mysql.connector
from mysql.connector import Error
import configparser
from datetime import datetime, timedelta



def getDataRegion(From,To):
    api_day_country = 'https://api.covid19tracking.narrativa.com/api/country/spain?date_from={}&date_to={}'.format(From,To)
    #api_day_region = 'https://api.covid19tracking.narrativa.com/api/country/spain/region/madrid?date_from=2020-04-30&date_to=2020-04-30'
    
    response = requests.get(api_day_country)
    list_sql = []
    if response.status_code == 200:
        print("API Connected")
        data = json.loads(response.content)
        #loop days (from, to)
        for d in data['dates']:
            print("Processing: ", d)
            #loop regions (ccaa)
            for r in data['dates'][d]['countries']['Spain']['regions']:
                date = r['date']
                r_name = r['name']
                r_source = r['source']
                r_confirmed = r['today_confirmed']
                r_deaths = r['today_deaths']
                r_uci = r['today_intensive_care']
                r_recovered= r['today_recovered']
                r_new_confirmed = r['today_new_confirmed']
                r_new_deaths = r['today_new_deaths']
                r_new_uci = r['today_new_intensive_care']
                r_new_recovered = r['today_new_recovered']
                
                t = (date,r_name,r_source,r_confirmed,r_deaths,r_uci,r_recovered,r_new_confirmed,r_new_deaths,r_new_uci,r_new_recovered)
                list_sql.append(t)
            
        return list_sql
  
    else:
        print(response.status_code)
        print("Api Error")

def connectMySql(query,records):
   config = configparser.RawConfigParser()
   config.read('/Users/alvaro.maroto/Desktop/PersonalProjects/covid19/config.properties')
   data_conn = dict(config.items('MY_SQL'))
   try:
        connection = mysql.connector.connect(host='localhost',
                                         database= data_conn['database'],
                                         user= data_conn['user'],
                                         password= data_conn['password']
                                         )
        
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            cursor.executemany(query, records)
            connection.commit()
            print(cursor.rowcount, "Record inserted successfully into Laptop table")

   except Error as e:
        print("Error while connecting to MySQL", e)
   finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

def main():
    #Without arguments, From = To = Yesterday
    if len(sys.argv) == 1:
        From = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
        To = From
    
    elif len(sys.argv) == 3:
        From = sys.argv[1]
        To = sys.argv[2]
        
    else:
        print("Number incorrect of parameters")
        return
    
    print("From: {}, To: {}".format(From,To))
    records = getDataRegion(From,To)
    query = """insert into covid_day_ccaa (dt_date,ds_region,ds_source_data,qt_total_confirmed,qt_total_deaths,qt_total_uci,qt_total_recovered,qt_day_confirmed,
    qt_day_deaths,qt_day_uci,qt_day_recovered) VALUES (%s, %s, %s, %s,%s, %s, %s, %s,%s, %s, %s) 
    """
    print(records)
    connectMySql(query,records)
    
if __name__ == "__main__":
    main()